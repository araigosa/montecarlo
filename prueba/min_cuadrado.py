import numpy as np
from scipy.linalg import lu

"""1) FACTORIZACION QR"""

A=np.array([[4,0,1],[1,-5,1],[6,1,0],[1,-1,5]])
A

B= np.array([[9],[0],[0],[0]])
B

Q,R= np.linalg.qr(A)
Q

R

"""2) $\hat{x}=(A^{T}*A)^{-1}A^{T}b$, resolver el problema antrerior. y comprobar $\left ( A^{T} A \right )^{-1}A^{T}$"""

x= ((A.T@A)^-1)@A.T@B
x

"""Comprobar  con la pseudoinversa $\left ( A^{T} A \right )^{-1}A^{T}$"""

print (np.linalg.pinv(A))

pinvA= (A.T@A^-1)@A.T
