import sys
def fun_tasa_comision(x):
    if x<2:
        return 0
    elif x>=2 and x<5:
        return 0.02
    elif x>=5 and x<10:
        return 0.03
    else:
        return 0.04

"""# Sección nueva"""
print("SE INICIA EL PROGRAMA")
import numpy as np

avg=1
std=0.1
num_vendedores=int(sys.argv[1])
val_objetivos=[2,5,10]
prob_objetivos=[0.6,0.3,0.1]

pct_objetivo=np.random.normal(avg, std, num_vendedores).round(2)
objetivos=np.random.choice(val_objetivos,num_vendedores,p=prob_objetivos)
ventas=objetivos*pct_objetivo
pct_comision=[fun_tasa_comision(i) for i in ventas]
comision=pct_comision*ventas

print("Las ventas totales fueron: ",sum(ventas).round(2))
print("Las comisiones totales fueron: ",sum(comision).round(2))

# Creamos una función que nos calcule cuál es la comisión
def fun_tasa_comision(x):
    '''
    Si las ventas logradas son menores a 2' la comisión es del 0
    Si están entre 2' y 5' la comisión es 2%
    Si está entre 5' y 10' la comisión es 3%
    Si está por encima de 10' la comisión es del 4%
    '''
    if x<2:
        return 0
    elif x>=2 and x<5:
        return 0.02
    elif x>=5 and x<10:
        return 0.03
    else:
        return 0.04

# Asumimos que de los datos históricos de ventas ajustamos una
# distribución normal de las distancias del valor de las ventas logradas
# al objetivo. Puede ser que se acercó al objetivo y no lo logró o puede que lo haya superado un poco
avg=1
std=0.1

# Valores trimestrales de los objetivos en millones
val_objetivos=[2,5,10]

# La distribución de los objetivos a través de los vendedores no es ubiforme
# Mientras mayor sea el objetivo la probabilidad de encontrar un vendedor que lo logre es menor
prob_objetivos=[0.6,0.3,0.1]

datos=[]
simulaciones=1000
for i in range(simulaciones):
    pct_objetivo=np.random.normal(avg, std, num_vendedores).round(2)

    # escogemos aleatoriamente los objetivos logrados de todos los vendedores
    objetivos=np.random.choice(val_objetivos,num_vendedores,p=prob_objetivos)

    # Con esto ya podemos calcular las ventas
    ventas=objetivos*pct_objetivo

    # Calculamos el porcentaje de comisiones de todos los vendedores
    perc_comision=[fun_tasa_comision(i) for i in ventas]

    # y las comisiones
    comision=perc_comision*ventas

    # imprimimos la suma de las comisiones y las ventas
    datos.append([sum(comision).round(2),sum(ventas).round(2)])


#print(datos)

#np.array(datos).T.tolist()

import matplotlib.pyplot as plt
for i in range(len(datos)):
  point = datos[i] #the element ith in data
  x = point[1] #the first coordenate of the point, x
  y = point[0] #the second coordenate of the point, y
  plt.scatter(x,y) #plot the point
plt.savefig(sys.argv[1]+ "VentasVsComimsiones.png")
